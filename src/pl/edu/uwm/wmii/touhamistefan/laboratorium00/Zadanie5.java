package pl.edu.uwm.wmii.touhamistefan.laboratorium00;

public class Zadanie5 {
    public static void main(String[] args) {
        String tekst = "Java";
        System.out.print(_hr(tekst.length()));
        System.out.print("\n|");
        System.out.print(tekst + "|\n");
        System.out.print(_hr(tekst.length()));
    }

    private static String _hr(int length) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i <= length + 1; i++)
            str.append((i == 0 || i == length + 1) ? '+' : '-');
        return str.toString();
    }
}
